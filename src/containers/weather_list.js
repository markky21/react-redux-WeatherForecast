import React, {Component} from 'react';
import {connect} from 'react-redux';
import GoogleMap from '../components/google_map';


import Char from '../components/char';

class WeatherList extends Component {

    renderWeather(cityData) {

        const cityName = cityData.city.name;
        const weather = {
            temp: cityData.list.map((weather) => weather.main.temp - 272.15),
            pressure: cityData.list.map((weather) => weather.main.pressure),
            humidity: cityData.list.map((weather) => weather.main.humidity),
        };
        const {lat, lon: lng} = cityData.city.coord;

        return (
            <tr key={cityName + Math.random()}>
                <td><GoogleMap lat={lat} lng={lng}/></td>
                <td><Char data={weather.temp} color='orange' unit='&#8451;'/></td>
                <td><Char data={weather.pressure} color='green' unit='hPa;'/></td>
                <td><Char data={weather.humidity} color='blue' unit='%'/></td>
            </tr>
        )
    }

    render() {
        return (
            <div className="row weather-list">
                <table className="table table-hover col-12">
                    <thead>
                    <tr>
                        <th>City</th>
                        <th>Temperature [&#8451;]</th>
                        <th>Pressure [hPa]</th>
                        <th>Humidity [%]</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.props.weather.map(this.renderWeather)}
                    </tbody>
                </table>
            </div>
        );
    }

}

function mapStateToProps({weather}) {
    return {weather};
}

export default connect(mapStateToProps)(WeatherList);