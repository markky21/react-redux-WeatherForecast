import React from 'react';
import {Sparklines, SparklinesLine, SparklinesReferenceLine} from 'react-sparklines';
import _ from 'lodash';

function average(data) {
    return _.round(_.sum(data) / data.length);
}

export default (props) => {

    return (
        <article>
            <Sparklines data={props.data} height={props.height || 100} width={props.width || 150}>
                <SparklinesLine color={props.color || 'blue'}/>
                <SparklinesReferenceLine type="avg"/>
            </Sparklines>
            <div>avg for next 5 days: {average(props.data)} {props.unit || ''}</div>
        </article>
    )

}