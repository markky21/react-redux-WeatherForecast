import React, {Component} from 'react';
import SearchBar from '../containers/search_bar';
import WeatherList from '../containers/weather_list';

export default class App extends Component {
    render() {
        return (
            <div className='container' style={{marginTop: '50px'}}>
                <div className="row">
                    <nav className="col-12">
                        <SearchBar/>
                    </nav>
                    <section className="col-12">
                        <WeatherList/>
                    </section>
                </div>
            </div>
        );
    }
}
